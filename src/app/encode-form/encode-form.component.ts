import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-encode-form',
  templateUrl: './encode-form.component.html',
  styleUrls: ['./encode-form.component.css']
})
export class EncodeFormComponent implements OnInit {
  result = null;
  encodeForm = new FormGroup({
    encode: new FormControl('', [Validators.required, this.Validate])
  });
  constructor(private apiService: ApiService) {}
  ngOnInit() {}
  Validate(control: AbstractControl) {
    if (
      control.value &&
      control.value.toString().search(/[^a-f]|[^A-F]|\d/i) > -1
    ) {
      return { forbiddenChar: true };
      // return null;
    }
    return null;
    // return { forbiddenChar: true };
  }

  onSubmit() {
    if (this.encodeForm.valid) {
      this.apiService
        .get('/compress', { text: this.encodeForm.value.encode })
        .subscribe((res: { string: string }) => {
          this.result = res.string;
        });
    }
  }
  clearForm() {
    this.encodeForm.reset();

    this.result = null;
  }
}
