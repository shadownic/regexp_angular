import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'http://localhost:8000';
  constructor(private http: HttpClient) {}

  get(path, params?: { [param: string]: string | string[] }): Observable<any> {
    return this.http.get(`${this.url}${path}`, {
      responseType: 'json',
      params
    });
  }
}
