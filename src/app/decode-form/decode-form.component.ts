import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-decode-form',
  templateUrl: './decode-form.component.html',
  styleUrls: ['./decode-form.component.css']
})
export class DecodeFormComponent implements OnInit {
  result = null;
  decodeForm = new FormGroup({
    decode: new FormControl('', [Validators.required, this.Validate])
  });
  constructor(private apiService: ApiService) {}
  // this should be seporate service actually
  Validate(control: AbstractControl) {
    if (control.value) {
      const string = control.value.toString();
      if (string.search(/(^[a-f]0)/) > -1) {
        return { zeroStart: true };
      }
      if (string.search(/^\d/) > -1) {
        return { insStart: true };
      }
      if (string.search(/[^a-f\d]/i) > -1) {
        return { forbiddenChar: true };
      }
    }

    return null;
  }
  ngOnInit() {}

  onSubmit() {
    // here i should make validation for response status but i dont want to bother with interfaces
    if (this.decodeForm.valid) {
      this.apiService
        .get('/decompress', { text: this.decodeForm.value.decode })
        .subscribe((res: { string: string }) => {
          this.result = res.string;
        });
    }
  }
  clearForm() {
    this.decodeForm.reset();
    this.result = null;
  }
}
